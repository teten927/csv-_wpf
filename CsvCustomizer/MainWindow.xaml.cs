﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;

namespace CsvCustomizer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private bool IsInitTextBoxFileName = true;  // テキストボックスが初期値かどうかを判定するメンバ
        private string FileName;                    // CSVファイル名を保存するメンバ
        private Point? StartPoint = null;                   // ドラッグ開始座標を保存するメンバ
        private List<ListBoxItem> Items;            // ドロップ先に渡すデータを格納するメンバ

        // ListBoxItemインスタンスのコピーを作成
        private ListBoxItem CopyListBoxItem(ListBoxItem Item)
        {
            var NewItem = new ListBoxItem();
            NewItem.Content = Item.Content;
            return NewItem;
        }

        // ヘッダ行のテキストボックスに文字を入力したときの処理
        private void HeaderColumnInput(object sender, TextCompositionEventArgs e)
        {
            // 1~99以外の値ははじく
            Regex regex = new Regex("^[1-9][0-9]?$");
            var text = textBox_headerColumn.Text + e.Text;
            e.Handled = !regex.IsMatch(text);
        }

        // ヘッダなしのチェックボックスがチェックされたときの処理
        private void CheckboxExistHeaderChecked(object sender, RoutedEventArgs e)
        {
            // テキストボックスを無効化し、背景をグレー化する
            textBox_headerColumn.IsEnabled = false;
            textBox_headerColumn.Background = Brushes.Gray;
        }

        // ヘッダなしのチェックボックスのチェックが外されたときの処理
        private void CheckboxExistHeaderUnchecked(object sender, RoutedEventArgs e)
        {
            // テキストボックスの状態を通常に戻す
            textBox_headerColumn.IsEnabled = true;
            textBox_headerColumn.Background = Brushes.Transparent;
        }

        // 「参照」ボタンをクリックしたときの処理
        private void ReferenceClick(object sender, RoutedEventArgs e)
        {
            // ダイアログのインスタンスを生成
            var Dialog = new OpenFileDialog();

            // ファイルの種類を設定
            Dialog.Filter = "CSVファイル (*.csv)|*.csv|全てのファイル (*.*)|*.*";

            // ダイアログを表示する
            if (Dialog.ShowDialog() == true)
            {
                // ファイルが選択されると、選択されたファイル名がテキストボックスに入る
                textBox_fileName.Foreground = new SolidColorBrush(Colors.Black);
                textBox_fileName.Text = Dialog.FileName;
                this.IsInitTextBoxFileName = false;
            }

        }

        // ファイル名テキストボックスにフォーカスが当たった時の処理
        private void FileNameGotFocus(object sender, RoutedEventArgs e)
        {
            var Tb = (TextBox)sender as TextBox;

            // 初期状態を解除する
            if (this.IsInitTextBoxFileName)
            {
                Tb.Clear();
                Tb.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        // ファイル名テキストボックスからフォーカスが外れた時の処理
        private void FileNameLostFocus(object sender, RoutedEventArgs e)
        {
            var Tb = (TextBox)sender as TextBox;

            // テキストボックスが空ならば、初期状態に戻す
            if (Tb.Text.Equals(""))
            {
                Tb.Text = "ファイル名";
                Tb.Foreground = new SolidColorBrush(Colors.LightGray);
                this.IsInitTextBoxFileName = true;

            }
        }

        // 「決定」ボタンをクリックしたときの処理
        private void DecideClick(object sender, RoutedEventArgs e)
        {

            listBox_left.Items.Clear();
            listBox_right.Items.Clear();

            // テキストボックスが空ならば、エラーメッセージを通知
            if (this.IsInitTextBoxFileName)
            {
                MessageBox.Show("ファイル名を入力してください。");
                return;
            }

            StreamReader InputCsvFile;

            // CSVファイルを読み込む処理（存在しないファイルの場合エラーメッセージを通知）
            try
            {
                InputCsvFile = new StreamReader(textBox_fileName.Text, System.Text.Encoding.GetEncoding("shift_jis"));
            }
            catch (Exception)
            {
                MessageBox.Show("ファイルが存在しません。入力しなおしてください。");
                return;
            }

            // ヘッダ項目一覧を作成（ヘッダなしCSVファイルの場合は、〇列目をヘッダにする）
            var InputCsvLine = InputCsvFile.ReadLine();
            if (InputCsvLine == null)
            {
                MessageBox.Show("ファイルにデータが存在しません。");
                return;
            }
            var HeaderList = new List<string>();
            if (checkBox_notExistHeader.IsChecked == true)
            {
                var HeaderArray = InputCsvLine.Split(',');
                for (int I = 1; I <= HeaderArray.Length; I++)
                {
                    HeaderList.Add(I + "列目");
                }
            }
            else
            {
                // テキストボックスで指定したヘッダ行まで読み飛ばす
                for (int I = 1; I < int.Parse(textBox_headerColumn.Text); I++)
                {
                    InputCsvLine = InputCsvFile.ReadLine();
                    if (InputCsvLine == null)
                    {
                        MessageBox.Show("ヘッダ行の指定に誤りがあります。");
                        return;
                    }
                }
                var HeaderArray = InputCsvLine.Split(',');
                HeaderList.AddRange(HeaderArray);
            }

            // CSVファイルを読み込めた場合、ファイル名を保存
            this.FileName = textBox_fileName.Text;

            // ヘッダ情報一覧を見える化
            grid_display_after.Visibility = Visibility;

            foreach (string Header in HeaderList)
            {
                var Item = new ListBoxItem();
                Item.Content = Header;
                listBox_left.Items.Add(Item);
            }

            InputCsvFile.Close();
        }

        // 「▶」ボタンが押された時
        private void RightClick(object sender, RoutedEventArgs e)
        {
            var Items = listBox_right.Items;
            var TmpStrings = new List<string>();

            // 右側のリストに既存のものを再入しないための文字列配列（右と左のリストは、文字列が同一の異なるオブジェクトになるため）
            foreach (ListBoxItem Item in listBox_right.Items)
            {
                TmpStrings.Add((string)Item.Content);
            }

            // 左側のリストの順番と同じにしたいので、全リスト項目を順にとる
            foreach (ListBoxItem Item in listBox_left.Items)
            {
                // (選択されており) かつ (まだ右側のリストにない項目) のみ、右側のリストに移動できる
                if (listBox_left.SelectedItems.Contains(Item) && !(TmpStrings.Contains(Item.Content)))
                {
                    var NewItem = CopyListBoxItem(Item);
                    Items.Add(NewItem);
                    listBox_right.SelectedItems.Add(NewItem);
                }
            }

            // 左側のリストの選択を解除する
            listBox_left.SelectedItems.Clear();

        }

        // 「▲」ボタンが押された時
        private void UpClick(object sender, RoutedEventArgs e)
        {
            var SelectedItems = listBox_right.SelectedItems;    // 選択されている項目群
            var Items = listBox_right.Items;                    // 項目群

            // (選択されている項目の先頭がリストの先頭の項目) または (選択されている項目がリストの項目すべて) の場合は処理しない
            if (listBox_right.SelectedIndex <= 0 || SelectedItems.Count == Items.Count) return;

            var Indexs = new List<int>();   // 選択された項目のインデックス

            foreach (ListBoxItem Item in SelectedItems)
            {
                Indexs.Add(Items.IndexOf(Item));
            }

            SelectedItems.Clear();

            Indexs.Sort();

            // 選択された項目のコピーを一つ上にインサートし、元の項目を削除することで、上へスライドしたように見える
            foreach (int Index in Indexs)
            {
                var TmpItem = (ListBoxItem)Items[Index];
                var NewItem = CopyListBoxItem(TmpItem);
                Items.Insert(Index - 1, NewItem);
                Items.Remove(TmpItem);
                SelectedItems.Add(NewItem);
            }
        }

        // 「▼」ボタンが押された時
        private void DownClick(object sender, RoutedEventArgs e)
        {
            var SelectedItems = listBox_right.SelectedItems;    // 選択されている項目群
            var Items = listBox_right.Items;                    // 項目群

            // (選択されている項目に末尾の項目がある) または (選択されている項目がリストの項目すべて) の場合は処理しない
            if (SelectedItems.Contains(Items[Items.Count - 1]) || SelectedItems.Count == Items.Count) return;


            var Indexs = new List<int>();   // 選択された項目のインデックス

            foreach (ListBoxItem Item in SelectedItems)
            {
                Indexs.Add(Items.IndexOf(Item));
            }

            SelectedItems.Clear();

            Indexs.Sort();
            Indexs.Reverse();

            // 元の項目をコピーし、削除し、コピーを一つ下にインサートすることで、下へスライドしたように見える
            foreach (int Index in Indexs)
            {
                var TmpItem = (ListBoxItem)Items[Index];
                var NewItem = CopyListBoxItem(TmpItem);
                Items.Remove(TmpItem);
                Items.Insert(Index + 1, NewItem);
                SelectedItems.Add(NewItem);
            }
        }

        // 「全選択」ボタンが押された時
        private void AllSelectClick(object sender, RoutedEventArgs e)
        {
            listBox_right.Items.Clear();
            listBox_right.SelectedItems.Clear();

            foreach (ListBoxItem Item in listBox_left.Items)
            {
                var NewItem = CopyListBoxItem(Item);
                listBox_right.Items.Add(NewItem);
                listBox_right.SelectedItems.Add(NewItem);
            }

            listBox_left.SelectedItems.Clear();
        }

        // 「削除」ボタンが押された時
        private void DeleteClick(object sender, RoutedEventArgs e)
        {
            while (listBox_right.SelectedItems.Count > 0)
            {
                listBox_right.Items.Remove(listBox_right.SelectedItems[0]);
            }
        }

        // 「実行」ボタンが押された時
        private void ProceedClick(object sender, RoutedEventArgs e)
        {
            var TmpStrings = new List<string>();

            // 左側と右側のリストを比較するための文字列配列（オブジェクトの比較では異なる結果となるため）
            foreach (ListBoxItem Item in listBox_left.Items)
            {
                TmpStrings.Add((string)Item.Content);
            }

            var Indexs = new List<int>();   // 列番号の数列

            foreach (ListBoxItem Item in listBox_right.Items)
            {
                Indexs.Add(TmpStrings.IndexOf((string)Item.Content));
            }

            StreamReader InputCsvFile;
            StreamWriter OutputCsvFile;

            var OutputCsvFilePath = ".\\" + this.FileName.Split('\\').Last().Split('.')[0] + "_customized(" + DateTime.Now.ToString("yyyyMMddHHmmss") + ").csv"; // 出力ファイルパス

            // CSVファイルを読み込む処理（存在しないファイルの場合エラーメッセージを通知）
            try
            {
                InputCsvFile = new StreamReader(this.FileName, System.Text.Encoding.GetEncoding("shift_jis"));
                OutputCsvFile = new StreamWriter(OutputCsvFilePath, true, System.Text.Encoding.GetEncoding("shift_jis"));
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("ファイルが存在しません。入力しなおしてください。");
                return;
            }

            string Line;

            // ファイルを一行ずつ読み込み、カスタマイズして書き込む
            while ((Line = InputCsvFile.ReadLine()) != null)
            {
                var SplitedLine = Line.Split(',');
                var TmpArray = new List<string>();
                foreach (int Index in Indexs)
                {
                    TmpArray.Add(SplitedLine[Index]);
                }
                OutputCsvFile.WriteLine(string.Join(",", TmpArray));
            }

            InputCsvFile.Close();
            OutputCsvFile.Close();

            MessageBox.Show("ファイルのカスタマイズに成功しました。");

            this.Close();

        }

        // 左側のリストの項目がクリックされ、ドラッグ＆ドロップが始まった時
        private void ListBoxLeftPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var Lb = sender as ListBox;

            if (Lb.SelectedItems.Count <= 0) return; // 選択されていない場合返す

            // ドラッグ開始座標メンバに値を格納
            this.StartPoint = Lb.PointFromScreen(e.GetPosition(Lb));

            var Items = new List<ListBoxItem>();
            var TmpStrings = new List<string>();

            // 右側のリストに既存のものを再入しないための文字列配列（右と左のリストは、文字列が同一の異なるオブジェクトになるため）
            foreach (ListBoxItem Item in listBox_right.Items)
            {
                TmpStrings.Add((string)Item.Content);
            }

            // 左側のリストの順番と同じにしたいので、全リスト項目を順にとる
            foreach (ListBoxItem Item in Lb.Items)
            {
                // (選択されており) かつ (まだ右側のリストにない項目) のみ、右側のリストに移動できる
                if (Lb.SelectedItems.Contains(Item) && !(TmpStrings.Contains(Item.Content)))
                {
                    Items.Add(CopyListBoxItem(Item));
                }
            }

            // ドロップ先に引き渡すデータのメンバに格納
            this.Items = Items;
        }

        // 右側のリストの項目がクリックされ、ドラッグ＆ドロップが始まった時
        private void ListBoxRightPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var Lb = sender as ListBox;

            if (Lb.SelectedItems.Count <= 0) return; // 選択されていない場合返す

            // スクロールバーをクリックした場合は、対象外とする
            if (e.OriginalSource.ToString() == "System.Windows.Shapes.Rectangle") return;

            // ドラッグ開始座標メンバに値を格納
            this.StartPoint = Lb.PointToScreen(e.GetPosition(Lb));

            var Items = new List<ListBoxItem>();

            // 左側のリストの順番と同じにしたいので、全リスト項目を順にとる
            foreach (ListBoxItem Item in Lb.Items)
            {
                // 選択されているもののみ、ドラッグできる
                if (Lb.SelectedItems.Contains(Item)) Items.Add(CopyListBoxItem(Item));
            }

            // ドロップ先に引き渡すデータのメンバに格納
            this.Items = Items;
        }

        // ドラッグ開始とする距離を移動したかどうかの判定を行う。
        private bool IsDragStartable(Vector delta)
        {
            return (SystemParameters.MinimumHorizontalDragDistance < Math.Abs(delta.X)) ||
                   (SystemParameters.MinimumVerticalDragDistance < Math.Abs(delta.Y));
        }

        // リストボックス内でマウスが動いた時の処理（ドラッグ＆ドロップが行われていることの判定）
        private void ListBoxPreviewMouseMove(object sender, MouseEventArgs e)
        {
            var Lb = sender as ItemsControl;

            if (Lb == null || this.StartPoint == null) return;

            var MousePoint = Lb.PointToScreen(e.GetPosition(Lb));

            // 一定距離マウスが動いたとき、ドラッグ ＆ ドロップの開始
            if (IsDragStartable((MousePoint - (Point)this.StartPoint)))
            {
                this.StartPoint = null;
                DragDrop.DoDragDrop(Lb, this.Items, DragDropEffects.Move);
            }
        }

        // マウス左ボタンが押上された時
        private void ListBoxPreviewLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // 全ての値をクリア
            this.Items = null;
            this.StartPoint = null;
        }

        // 右側のリストにドロップされた時
        private void ListBoxRightDrop(object sender, DragEventArgs e)
        {
            var Lb = sender as ListBox;

            var Items = (List<ListBoxItem>)e.Data.GetData(typeof(List<ListBoxItem>));

            var ToRemoveItems = new List<ListBoxItem>();

            // 右側のリストに既存のものを削除するための配列を作成
            foreach (ListBoxItem Item in Items)
            {
                foreach (ListBoxItem TmpItem in Lb.Items)
                {
                    if (Item.Content == TmpItem.Content)
                    {
                        ToRemoveItems.Add(TmpItem);
                        break;
                    }
                }
            }

            var MousePoint = Lb.PointToScreen(e.GetPosition(Lb)); // マウスの座標
            int Index = -1; // ドロップした位置が右側リストの何番目の項目の上の位置かを格納

            // indexに値を格納
            foreach (ListBoxItem Item in Lb.Items)
            {
                Point TmpPoint;
                // リストボックスの項目の座標
                TmpPoint = Item.PointToScreen(new Point(0.0d, 0.0d));

                // リストボックスの項目のトップ座標が、マウス座標以下のとき
                if (MousePoint.Y <= TmpPoint.Y)
                {
                    // マウスポインタがあるリストボックスの項目の位置 + 1がIndexに入る
                    Index = Lb.Items.IndexOf(Item);
                    // 右側のリスト内の移動で、上から下へドラッグするときと下から上へドラッグするときで処理を分ける
                    if (ToRemoveItems.Count == 0 || Index <= Lb.Items.IndexOf(ToRemoveItems[0])) Index -= 1;
                    break;
                }
            }

            Lb.SelectedItems.Clear();

            foreach (ListBoxItem Item in Items)
            {
                if (Index == -1) Lb.Items.Add(Item);
                else
                {
                    Lb.Items.Insert(Index, Item);
                    Index++;
                }
                Lb.SelectedItems.Add(Item);
            }

            listBox_left.SelectedItems.Clear();

            // 重複しないように削除
            foreach (ListBoxItem Item in ToRemoveItems) Lb.Items.Remove(Item);
        }
    }
}